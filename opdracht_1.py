#import alle modules
import tkinter
import re
import sys
from tkinter import filedialog
import math

#openen van file explorer
root_window = tkinter.Tk()
root_window.withdraw()

def choose_file():
    return filedialog.askopenfilename()

#het geselecteerde bestand een variable geven
selected_file = choose_file()

#Open het bestand
file = open(selected_file, "r")

#Maak arraylist aan
courses = []

#Maak dictionary aan
d = {}

#Maak string aan.
courses_string = ''



#Loop door alle regels in het bestand
for line in file:

    #Kijk of het bestand niet leeg is.
    if line == "":
        print("Dit bestand is leeg.")
    else:
        x = line.split()
        y = x[0]
        z = x[1]

        #Split alle klassen zodat ze allemaal los staan.
        courses = z.split("+")

        #Loop door alle klassen en zet ze in een string
        for elements in courses:
         courses_string = courses_string + elements + ' '
      
#Zet alle klassen in een array
courses = courses_string.split(' ')

print("Klassen  |  Aantal leerlingen  |  Min aantal klassen   |  Max aantal klassen  |  Voorstel")

#Verwijder de onodige informatie die nog in de lijst array staat.
del courses[0]
del courses[680]

#Loop door alle klassen die in de array staan en voeg ze toe aan een dictionary met als key de klas naam en als value het aantal leerlingen.
#Wanneer de klas al bestaat voegt hij een leerling toe aan de lijst. Wanneer de klas nog niet bestaat voegt hij de klas toe en zet hem op 1.
for element in courses:
    if element in d:
        d[element] = d.get(element)+1
    else:
        d[element] = 1
        

#Loop door alle onderdelen in de dictionary. c = de classes. s = het aantal students.
for klassen,leerlingen in d.items():

    #Maak een integer van de string.
    Klassen_int = int(leerlingen)

    #Check of het aantal leerlingen in de klas kleiner is dan 30.
    if Klassen_int < 30:
            print (str(klassen), '  |  ', str(leerlingen), '  |  1 *', math.ceil(Klassen_int/1), '|  1 *', math.ceil(Klassen_int/1), '  |  1')
    if Klassen_int == 30:
            print (str(klassen), '  |  ', str(leerlingen), '  |  2 *', math.ceil(Klassen_int/2), '|  2 *', math.ceil(Klassen_int/2), '  |  2')
    if (Klassen_int > 30) and (Klassen_int < 69):
        if Klassen_int >= 20:
            print (str(klassen), '  |  ', str(leerlingen), '  |  3 *', math.ceil(Klassen_int/3), '|  2 *', math.ceil(Klassen_int/2), '  |  3')
        else:
            print (str(klassen), '  |  ', str(leerlingen), '  |  3 *', math.ceil(Klassen_int/3), '|  2 *', math.ceil(Klassen_int/2), '  |  2')
    if (Klassen_int >= 69) and (Klassen_int < 92):
        if Klassen_int >= 20:
            print (str(klassen), '  |  ', str(leerlingen), '  |  4 *', math.ceil(Klassen_int/4), '|  5 *', math.ceil(Klassen_int/5), '  |  5')
        else:
            print (str(klassen), '  |  ', str(leerlingen), '  |  4 *', math.ceil(Klassen_int/4), '|  5 *', math.ceil(Klassen_int/5), '  |  4')
    if Klassen_int >= 92:
        if Klassen_int >= 20:
            print (str(klassen), '  |  ', str(leerlingen), '  |  5 *', math.ceil(Klassen_int/5), '|  6 *', math.ceil(Klassen_int/6), '  |  6')
        else:
            print (str(klassen), '  |  ', str(leerlingen), '  |  5 *', math.ceil(Klassen_int/5), '|  6 *', math.ceil(Klassen_int/6), '  |  5')

file.close()